package ru.t1.aayakovlev.tm.command.task;

import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import java.util.Arrays;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;
import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextNumber;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Change task status by index.";

    public static final String NAME = "task-change-status-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        System.out.println("Statuses to enter:");
        System.out.println(Arrays.toString(Status.values()));
        System.out.print("Enter new status: ");
        final String statusValue = nextLine();
        final Status status = Status.toStatus(statusValue);
        getTaskService().changeStatusByIndex(index, status);
    }

}
