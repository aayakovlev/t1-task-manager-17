package ru.t1.aayakovlev.tm.command.system;

import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.FormatUtil.format;

public final class SystemShowInfoCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-i";

    public static final String DESCRIPTION = "Show hardware info.";

    public static final String NAME = "info";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        final Runtime runtime = Runtime.getRuntime();
        final long availableCores = runtime.availableProcessors();
        final long maxMemory = runtime.maxMemory();
        final long freeMemory = runtime.freeMemory();
        final long totalMemory = runtime.totalMemory();
        final long usedMemory = totalMemory - freeMemory;

        final boolean checkMaxMemory = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = checkMaxMemory ? "no limit" : format(maxMemory);
        final String freeMemoryFormat = format(freeMemory);
        final String totalMemoryFormat = format(totalMemory);
        final String usedMemoryFormat = format(usedMemory);

        System.out.println("[INFO]");
        System.out.println("Available cores: " + availableCores);
        System.out.println("Max memory: " + maxMemoryFormat);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Used memory: " + usedMemoryFormat);
    }

}
