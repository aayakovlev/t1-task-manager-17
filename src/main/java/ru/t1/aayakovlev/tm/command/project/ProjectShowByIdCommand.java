package ru.t1.aayakovlev.tm.command.project;

import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.Project;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Show project by id.";

    public static final String NAME = "project-show-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        final Project project = getProjectService().findById(id);
        showProject(project);
    }

}
