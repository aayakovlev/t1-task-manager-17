package ru.t1.aayakovlev.tm.command.task;

import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextNumber;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Start task by index.";

    public static final String NAME = "task-start-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START TASK BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        getTaskService().changeStatusByIndex(index, Status.IN_PROGRESS);
    }

}
