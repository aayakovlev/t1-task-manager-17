package ru.t1.aayakovlev.tm.service;

public interface ServiceLocator {

    CommandService getCommandService();

    LoggerService getLoggerService();

    ProjectService getProjectService();

    ProjectTaskService getProjectTaskService();

    TaskService getTaskService();

}
